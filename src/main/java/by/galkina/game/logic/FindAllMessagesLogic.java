package by.galkina.game.logic;


import by.galkina.game.dao.impl.MessageDao;
import by.galkina.game.entity.Message;
import by.galkina.game.exception.DAOException;
import by.galkina.game.exception.TechnicalException;

import java.util.Collections;
import java.util.List;

public class FindAllMessagesLogic {

    public static List<Message> find(Long id) throws TechnicalException {
        List<Message> messages;
        try {
            messages = MessageDao.getInstance().findByUserIdTo(id);
            Collections.reverse(messages);
        } catch (DAOException e) {
            throw new TechnicalException(e);
        }
        return messages;
    }
    public static int countUnreadMessages(List<Message> messages){
        int count = 0;
        for(Message message: messages){
            if(!message.isRead()){
                count++;
            }
        }
        return count;
    }
}
