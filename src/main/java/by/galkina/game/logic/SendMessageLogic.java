package by.galkina.game.logic;


import by.galkina.game.dao.impl.MessageDao;
import by.galkina.game.dao.impl.UserDao;
import by.galkina.game.entity.Message;
import by.galkina.game.entity.User;
import by.galkina.game.exception.DAOException;
import by.galkina.game.exception.TechnicalException;

import java.util.Date;

public class SendMessageLogic {
    private static final boolean read = false;
    public static void send(Long userIdTo, Long userIdFrom, String text) throws TechnicalException {
        try {
            User to = UserDao.getInstance().findById(userIdTo);
            User from = UserDao.getInstance().findById(userIdFrom);
            MessageDao.getInstance().add(new Message(text, from, to, new Date(), read));
        } catch (DAOException e) {
            throw new TechnicalException();
        }
    }
}
