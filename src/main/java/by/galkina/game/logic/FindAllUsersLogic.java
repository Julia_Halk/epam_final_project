package by.galkina.game.logic;

import by.galkina.game.dao.impl.UserDao;
import by.galkina.game.entity.User;
import by.galkina.game.exception.DAOException;
import by.galkina.game.exception.TechnicalException;

import java.util.List;

public class FindAllUsersLogic {
    public static List<User> find() throws TechnicalException {
        List<User> users = null;
        try {
            users = UserDao.getInstance().findAll();
        } catch (DAOException e) {
            throw new TechnicalException(e);
        }
        return users;
    }
}
