package by.galkina.game.entity;


import java.util.Date;

public class Message {
    private Long messageId;
    private Date date;
    private String text;
    private User from;
    private User to;
    private boolean read;

    public Message() {
    }

    public Message(String text, User from, User to, Date date, boolean read) {
        this.text = text;
        this.from = from;
        this.to = to;
        this.date = date;
        this.read = read;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Message(String text) {
        this.text = text;
    }

    public Long getMessageId() {
        return messageId;
    }

    public void setMessageId(Long messageId) {
        this.messageId = messageId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public User getFrom() {
        return from;
    }

    public void setFrom(User from) {
        this.from = from;
    }

    public boolean isRead() {
        return read;
    }

    public void setRead(boolean read) {
        this.read = read;
    }

    public User getTo() {
        return to;
    }

    public void setTo(User to) {
        this.to = to;
    }

    @Override
    public String toString() {
        return "{\"id\":\"" + messageId +
                "\", \"user\":\"" + from +
                "\", \"date\":\"" + date +
                "\", \"text\":\"" + text +
                "\"}";
    }
}
