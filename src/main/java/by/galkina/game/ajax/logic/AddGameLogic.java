package by.galkina.game.ajax.logic;


import by.galkina.game.ajax.util.JSONUtil;
import by.galkina.game.dao.impl.GameDao;
import by.galkina.game.dao.impl.UserDao;
import by.galkina.game.entity.Game;
import by.galkina.game.entity.User;
import by.galkina.game.exception.DAOException;
import by.galkina.game.exception.LogicException;
import by.galkina.game.exception.TechnicalException;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.util.Date;

public class AddGameLogic {
    private static final String WIN = "win";
    private static final String BET = "bet";
    private static final String GAMES = "games";

    public static void add(User user, String data, HttpSession session) throws LogicException, TechnicalException {
        JSONObject json;
        try {
            json = JSONUtil.stringToJson(data);
            boolean win = Boolean.parseBoolean(((String) json.get(WIN)).trim());
            BigDecimal bet = new BigDecimal(((String)(json.get(BET))).trim());
            BigDecimal score = user.getScore();
            Double rating = user.getRating();
            if(win){
                user.setScore(score.add(bet));
                user.setRating(rating+bet.doubleValue()/100);
            }
            else{
                user.setScore(score.subtract(bet));
                user.setRating(rating - bet.doubleValue()/100);
            }
            Game game = create(user, win, bet);
            GameDao.getInstance().add(game);
            session.setAttribute(GAMES, GameDao.getInstance().findByUser(user));
            UserDao.getInstance().updateScoreAndRating(user);
        } catch (ParseException e) {
            throw new LogicException();
        } catch (DAOException e) {
            throw new TechnicalException();
        }
    }
    private static Game create(User user, boolean win,BigDecimal bet){
        return new Game(user,new Date(), win, bet);
    }
}
