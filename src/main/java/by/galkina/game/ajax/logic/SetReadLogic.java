package by.galkina.game.ajax.logic;


import by.galkina.game.ajax.util.JSONUtil;
import by.galkina.game.dao.impl.MessageDao;
import by.galkina.game.exception.DAOException;
import by.galkina.game.exception.LogicException;
import by.galkina.game.exception.TechnicalException;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

public class SetReadLogic {
    private static final String USER = "user";
    private static final String TEXT = "text";

    public static void set(String requestData) throws LogicException, TechnicalException {
        JSONObject json = null;
        try {
            json = JSONUtil.stringToJson(requestData);
            Long userId = Long.parseLong((String) json.get(USER));
            String text = ((String) json.get(TEXT)).trim();
            MessageDao.getInstance().setRead(userId, text);
        } catch (ParseException e) {
            throw new LogicException();
        } catch (DAOException e) {
            throw new TechnicalException();
        }
    }
}
