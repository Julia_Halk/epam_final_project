package by.galkina.game.ajax.logic;

import by.galkina.game.ajax.util.JSONUtil;
import by.galkina.game.dao.impl.UserDao;
import by.galkina.game.exception.DAOException;
import by.galkina.game.exception.LogicException;
import by.galkina.game.exception.TechnicalException;
import by.galkina.game.logic.SendMessageLogic;
import by.galkina.game.manager.MessageManager;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

public class SetBanLogic {
    private static final String BAN = "ban";
    private static final String USER = "user";
    private static final Long ADMIN_ID = (long)1;

    public static void set(String data, String lang) throws LogicException, TechnicalException {
        try {
            JSONObject json = JSONUtil.stringToJson(data);
            boolean ban = Boolean.parseBoolean(((String) json.get(BAN)).trim());
            Long userIdTo = Long.parseLong(((String) json.get(USER)).trim());
            String text = MessageManager.getManagerByLocale(lang).getProperty(MessageManager.BAN_MESSAGE);
            if(ban){
                SendMessageLogic.send(userIdTo, ADMIN_ID,text);
            }
            UserDao.getInstance().changeBan(userIdTo, ban);
        } catch (ParseException e) {
            throw new LogicException();
        } catch (DAOException e) {
            throw new TechnicalException();
        }
    }
}
