package by.galkina.game.ajax.util;


import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


public class JSONUtil {
    private static final String COMMAND = "command";
    private static final String SUM = "sum";
    private static final String UPDATE = "update";
    private static final String ID = "id";
    private static final String TEXT = "text";

    public JSONUtil() {}

    public static String getCommand(JSONObject json) {
        return (String) json.get(COMMAND);
    }
    public static String getUpdateType(JSONObject json) {
        return (String)json.get(UPDATE);
    }
    public static JSONObject stringToJson(String data) throws ParseException {
        JSONParser jsonParser = new JSONParser();
        return (JSONObject) jsonParser.parse(data.trim());
    }
    public static Long jsonToId(JSONObject json) {
        return Long.parseLong((String)json.get(ID));
    }
    public static String jsonToReviewText(JSONObject json) {
        return ((String)json.get(TEXT)).trim();
    }
    public static String jsonToSum(JSONObject json) {
        return ((String) json.get(SUM)).trim();
    }

}
