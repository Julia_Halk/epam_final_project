package by.galkina.game.command.impl;

import by.galkina.game.command.Command;
import by.galkina.game.entity.User;
import by.galkina.game.exception.TechnicalException;
import by.galkina.game.logic.SendMessageLogic;
import by.galkina.game.manager.MessageManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class SendMessageCommand implements Command {
    private static final String USER = "user";
    private static final String USER_TO = "to";
    private static final String LANG = "lang";
    private static final String PARAM_PAGE = "toUserOffice";
    private static final String TEXT = "text";
    private static final String ERROR_MESSAGE = "errorMessage";
    private static final Long ADMIN_ID = (long)1;

    @Override
    public String execute(HttpServletRequest request) {
        String page;
        HttpSession session = request.getSession(true);
        String lang = (String)session.getAttribute(LANG);
        User user = (User)session.getAttribute(USER);
        String to = request.getParameter(USER_TO);
        Long userIdTo = Long.parseLong(request.getParameter(USER));
        String text = request.getParameter(TEXT);
        try {
            if(to.equals("user")){
                SendMessageLogic.send(userIdTo, ADMIN_ID, text);
            }
            else {
                SendMessageLogic.send(ADMIN_ID, user.getUserId(), text);
            }
        } catch (TechnicalException e) {
            request.setAttribute(ERROR_MESSAGE, MessageManager.getManagerByLocale(lang).getProperty(MessageManager.MESSAGE_WASNT_SENT));
        }
        page = ForwardCommand.checkPage(PARAM_PAGE, user);
        return page;
    }

}
