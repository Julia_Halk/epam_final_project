package by.galkina.game.command.impl;

import by.galkina.game.command.Command;
import by.galkina.game.exception.TechnicalException;
import by.galkina.game.logic.AddNewsLogic;
import by.galkina.game.manager.ConfigurationManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

public class AddNewsCommand implements Command {
    private static final Logger LOG = Logger.getLogger(AddNewsCommand.class);
    private static final String TITLE_RU = "title_ru";
    private static final String TITLE_EN = "title_en";
    private static final String TEXT_RU = "text_ru";
    private static final String TEXT_EN = "text_en";
    @Override
    public String execute(HttpServletRequest request) {
        String page = "";
        String titleRu = request.getParameter(TITLE_RU);
        String textRu= request.getParameter(TEXT_RU);
        String titleEn = request.getParameter(TITLE_EN);
        String textEn= request.getParameter(TEXT_EN);
        try {
            new AddNewsLogic().add(titleRu, textRu, titleEn, textEn);
        } catch (TechnicalException e) {
            LOG.error("Something has gone wrong.", e);
        }
        page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.ADMIN_OFFICE_PAGE_PATH);
        return page;
    }

}
