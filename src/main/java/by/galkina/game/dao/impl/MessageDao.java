package by.galkina.game.dao.impl;

import by.galkina.game.dao.IMessageDao;
import by.galkina.game.entity.Message;
import by.galkina.game.entity.User;
import by.galkina.game.exception.ConnectionPoolException;
import by.galkina.game.exception.DAOException;
import by.galkina.game.jdbc.ConnectionPool;
import by.galkina.game.jdbc.ConnectionWrapper;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class MessageDao implements IMessageDao {
    private static MessageDao instance = MessageDao.getInstance();
    private static final String FIND_BY_USER_TO = "SELECT * FROM complaint WHERE userId_to=?";
    private static final String INSERT_MESSAGE = "INSERT INTO complaint " +
            "(description, time, userId_from, userId_to) VALUES(?,?,?,?)";
    private static final String DELETE_MESSAGE = "DELETE FROM complaint WHERE userId_from=? AND description=?";
    private static final String READ_MESSAGE = "UPDATE complaint SET is_read = 1 WHERE userId_from = ? AND description = ?";

    private MessageDao(){}

    public static MessageDao getInstance() {
        if (instance == null) {
            instance = new MessageDao();
        }
        return instance;
    }
    public List<Message> findByUserIdTo(Long userId) throws DAOException {
        List<Message> messages = new ArrayList<>();
        ResultSet resultSet;
        try (ConnectionWrapper connectionWrapper = ConnectionPool.getInstance().takeConnection();
             PreparedStatement statement = connectionWrapper.prepareStatement(FIND_BY_USER_TO)) {
            statement.setLong(1, userId);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Date date = resultSet.getDate("time");
                Long userId_from = resultSet.getLong("userId_from");
                Long userId_to = resultSet.getLong("userId_to");
                String text = resultSet.getString("description");
                boolean read = resultSet.getBoolean("is_read");
                User userFrom = UserDao.getInstance().findById(userId_from);
                User userTo = UserDao.getInstance().findById(userId_to);
                Message message = new Message(text,userFrom, userTo,date,read);
                messages.add(message);
            }
        }
        catch (ConnectionPoolException | SQLException e) {
            throw new DAOException();
        }
        return messages;
    }

    public boolean add(Message entity) throws DAOException {
        try (ConnectionWrapper connectionWrapper = ConnectionPool.getInstance().takeConnection();
             PreparedStatement statement = connectionWrapper.prepareStatement(INSERT_MESSAGE)
        ) {
            statement.setString(1, entity.getText().trim());
            statement.setTimestamp(2, new java.sql.Timestamp(entity.getDate().getTime()));
            statement.setLong(3, entity.getFrom().getUserId());
            statement.setLong(4, entity.getTo().getUserId());
            statement.executeUpdate();
        } catch (SQLException ex) {
            throw new DAOException("SQLException in DAO layer!", ex);
        } catch (ConnectionPoolException ex) {
            throw new DAOException("ConnectionPool exception!", ex);
        }
        return true;
    }

    public boolean delete(Long userId, String text) throws DAOException {
        try (ConnectionWrapper connectionWrapper = ConnectionPool.getInstance().takeConnection();
             PreparedStatement statement = connectionWrapper.prepareStatement(DELETE_MESSAGE)
        ) {
            statement.setLong(1, userId);
            statement.setString(2, text);
            statement.executeUpdate();
        } catch (SQLException ex) {
            throw new DAOException("SQLException in DAO layer!", ex);
        } catch (ConnectionPoolException ex) {
            throw new DAOException("ConnectionPool exception!", ex);
        }
        return true;
    }

    public boolean setRead(Long userId, String text) throws DAOException {
        try (ConnectionWrapper connectionWrapper = ConnectionPool.getInstance().takeConnection();
             PreparedStatement statement = connectionWrapper.prepareStatement(READ_MESSAGE)
        ) {
            statement.setLong(1, userId);
            statement.setString(2, text.trim());
            statement.executeUpdate();
        } catch (SQLException ex) {
            throw new DAOException("SQLException in DAO layer!", ex);
        } catch (ConnectionPoolException ex) {
            throw new DAOException("ConnectionPool exception!", ex);
        }
        return true;
    }
}
