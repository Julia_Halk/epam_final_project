<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<fmt:setLocale value="${lang}" scope="session" />
<fmt:setBundle basename="messages" var="rb"/>
<html>
<head>
  <title><fmt:message key="page.user-office.title" bundle="${rb}"/></title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="../css/lib/bootstrap.min.css">
  <link rel="stylesheet" href="../css/main.css">
  <link rel="stylesheet" href="../css/office.css">
</head>
<body>
<div class="mainbody container-fluid">
  <div class="main_row">
    <div class="navbar-wrapper">
      <div class="container-fluid">
        <div class="navbar navbar-inverse navbar-static-top" role="navigation">
          <div class="container-fluid">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              </button>
              <a class="navbar-brand" href="#"><img src="../images/logo.png"> </a>
            </div>
            <div class="navbar-collapse collapse">
              <ul class="nav navbar-nav navbar-right">
                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <span class="user-avatar pull-left" style="margin-right:8px; margin-top:-5px;">
                                        <img src="../${user.photo}" class="img-responsive img-circle"  width="30px" height="30px">
                                    </span>
                                    <span class="user-name">
                                      ${user.username}
                                    </span>
                  <b class="caret"></b></a>
                  <ul class="dropdown-menu">
                    <li>
                      <div class="navbar-footer">
                        <div class="navbar-footer-content">
                          <ul>
                            <li><a href="controller?command=news"><fmt:message key="news" bundle="${rb}"/></a></li>
                            <c:if test="${user.score>=0.1}">
                              <c:if test="${!user.ban}">
                                <li><a href="controller?command=forward&forward=toGame"><fmt:message key="game.play" bundle="${rb}"/></a></li>
                              </c:if>
                            </c:if>
                            <c:if test="${user.score<0.1}">
                              <li><a><fmt:message key="office.add_money" bundle="${rb}"/></a></li>
                            </c:if>
                            <c:if test="${user.ban}">
                              <li><a><fmt:message key="office.ban" bundle="${rb}"/></a></li>
                            </c:if>
                            <li><a href="controller?command=logout"><i class="fa fa-power-off" aria-hidden="true"></i> <fmt:message key="button.logout" bundle="${rb}"/></a></li>
                          </ul>
                        </div>
                      </div>
                    </li>
                  </ul>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div style="padding-top:50px;"> </div>
    <div class="col-lg-3 col-md-3 hidden-sm hidden-xs">
      <div class="panel panel-default">
        <div class="panel-body">
          <div class="media">
            <div align="center">
              <img class="thumbnail img-responsive" id="upload" src="../${user.photo}" width="300px" height="300px">
              <div class="overlay"></div>
            </div>
            <form id="newImage" action='upload' method='post' enctype='multipart/form-data'>
              <label for="file">Choose File</label>
              <input type="file" name="file" id="file" accept="image/jpeg,image/png,image/gif"/>
              <input id="changePhoto" type="submit" value="<fmt:message bundle="${rb}" key="office.photo.download"/>" class="btn btn-default">
            </form>
            <div class="media-body">
              <h3><strong><fmt:message bundle="${rb}" key="register.gender"/></strong></h3>
              <c:if test="${user.gender=='MALE'}">
                <p><fmt:message bundle="${rb}" key="register.gender.male"/></p>
              </c:if>
              <c:if test="${user.gender=='FEMALE'}">
                <p><fmt:message bundle="${rb}" key="register.gender.female"/></p>
              </c:if>
              <hr>
              <h3><strong><fmt:message bundle="${rb}" key="register.date"/></strong></h3>
              <p><fmt:formatDate type="date" value="${user.birthday}" pattern="dd MMM yyyy"/></p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
      <div class="panel panel-default">
        <div class="panel-body">
          <div>
            <input type="text" id="username" value="${user.username}" disabled>
            <img id="edit_username" src="../images/edit.png" width="20px">
            <img id="save_username" src="../images/save.png" width="20px" hidden>
            <span style="float:right;"><ctg:today format="dd MMM yyyy"/></span>
          </div>
          <div class="dropdown pull-right">
            <button class="btn btn-black dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
              <fmt:message bundle="${rb}" key="office.add_money"/>
              <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
              <li id="addMoney"><a> <fmt:message bundle="${rb}" key="office.add_money"/></a></li>
              <li id="withdrawMoney"><a><fmt:message bundle="${rb}" key="office.withdraw"/></a></li>
              <li id="changePassword"><a><fmt:message bundle="${rb}" key="office.change.password"/></a></li>
              <li id="messages"><a><fmt:message bundle="${rb}" key="office.complaints"/> <span id="count">+${unread}</span></a></li>
            </ul>
          </div>
          <br>
          ${user.email}
          <br>
          <fmt:message bundle="${rb}" key="office.score"/>: ${user.score}$<br>
          <fmt:message bundle="${rb}" key="office.rating"/>:<fmt:formatNumber value="${user.rating}" pattern="###.##"/><br>
          <button class="btn btn-black open_window1" id="sendMsg"><fmt:message key="office.complaint" bundle="${rb}"/></button>
          <div class="popup2">
            <div class="close_window">x</div>
            <form action='controller' method='get'>
              <input type='hidden' name='command' value='sendMessage'>
              <input type='hidden' name='to' value='admin'>
              <input type="hidden" name="user" value="1">
              <textarea name='text' required></textarea><br>
              <input type='submit' class="btn btn-black" value='<fmt:message bundle="${rb}" key="button.send"/>'></form>
          </div>
        </div>
      </div>
      <hr>
    </div>
  </div>
  ${errorMessage}
  ${actionMessage}
  <div id="tab2" class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
    <div class="panel panel-default">
      <div class="panel-body">
          <div class="input-group">
            <span class="input-group-addon"><fmt:message key="office.enter-card-number" bundle="${rb}"/></span>
            <input type="text" class="form-control card" name="msg" placeholder="1111 1111 1111 1111"
                   maxlength="19" pattern="[0-9]{4}\s[0-9]{4}\s[0-9]{4}\s[0-9]{4}"  required>
          </div>
          <div class="input-group">
            <span class="input-group-addon"><fmt:message key="office.enter-the-amount" bundle="${rb}"/></span>
            <input type="number" class="form-control money" id="money" name="sum"
                   pattern="[1-9][0-9]{1,3}"
                   placeholder="0" required>
          </div>
          <br>
          <input type="button" id="upBalance" class="btn btn-black" value="<fmt:message key="office.send.amount" bundle='${rb}'/>"/>
      </div>
    </div>
  </div>
  <hr>
  <div id="tab3" class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
    <div class="panel panel-default">
      <div class="panel-body">
          <div class="input-group">
            <span class="input-group-addon"><fmt:message key="office.enter-card-number" bundle="${rb}"/></span>
            <input type="text" class="form-control card" name="msg" placeholder="1111 1111 1111 1111"
                   maxlength="19" pattern="[0-9]{4}\s[0-9]{4}\s[0-9]{4}\s[0-9]{4}"  required>
          </div>
          <div class="input-group">
            <span class="input-group-addon"><fmt:message key="office.enter-the-amount" bundle="${rb}"/></span>
            <input type="number" class="form-control money"  name="sum" id="sum"
                   pattern="[1-9][0-9]{1,3}"
                   placeholder="0" required>
          </div>
          <br>
          <input type="button" id="downBalance" class="btn btn-black" value="<fmt:message key="office.withdraw" bundle='${rb}'/>"/>
      </div>
    </div>
  </div>
  <div id="tab5" class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
    <div class="panel panel-default">
      <div class="panel-body">
        <form action="controller" method="post" class="pay form-group">
          <input type="hidden" name="command" value="changePassword">
          <div class="row top-margin">
            <div class="col-sm-6">
              <label><fmt:message bundle="${rb}" key="register.placeholder.password"/></label>
              <input type="password" name="password" class="form-control"
                     placeholder="<fmt:message key="register.placeholder.password" bundle="${rb}"/>"
                     pattern="^[а-яА-ЯёЁa-zA-Z0-9]{4,19}$"
                     data-tooltip="<fmt:message key="register.help.password" bundle="${rb}"/>">
            </div>
            <div class="col-sm-6">
              <label><fmt:message bundle="${rb}" key="register.title.password.again"/></label>
              <input type="password" name="passwordAgain" class="form-control"
                     placeholder="<fmt:message key="register.placeholder.password" bundle="${rb}"/>"
                     pattern="^[а-яА-ЯёЁa-zA-Z0-9]{4,19}$"
                     data-tooltip="<fmt:message key="register.help.password" bundle="${rb}"/>">
            </div>
          </div>
          <label><fmt:message bundle="${rb}" key="register.placeholder.password"/></label>
          <input type="password" name="newPassword" class="form-control"
                 placeholder="<fmt:message key="register.placeholder.password" bundle="${rb}"/>"
                 pattern="^[а-яА-ЯёЁa-zA-Z0-9]{4,19}$"
                 data-tooltip="<fmt:message key="register.help.password" bundle="${rb}"/>">
          <input type="submit" class="btn btn-black" value="<fmt:message key="office.change.password" bundle='${rb}'/>"/>
        </form>
      </div>
    </div>
  </div>
  <div id="tab6" class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
    <div class="panel panel-default">
      <div class="panel-body">
        <div>
          <c:if test="${messages.isEmpty()}">
            <fmt:message bundle="${rb}" key="office.label.empty_message_list"/>
          </c:if>
          <table id = "complaints">
            <c:forEach var="message" items="${messages}">
              <c:if test="${message.read}">
                <tr class="complaint read">
              </c:if>
              <c:if test="${!message.read}">
                <tr class="complaint">
              </c:if>
              <td class="date"><fmt:formatDate value="${message.date}" pattern="dd MMM yyyy"/></td>
              <td class="text">
                <c:if test="${message.text.length()>=30}">
                  ${message.text.substring(0,30)}...
                </c:if>
                <c:if test="${message.text.length()<30}">
                  ${message.text}
                </c:if>
              </td>
              <td class="hidden-text" hidden>${message.text}</td>
              <td class="user" hidden>${message.from.userId}</td>
              </tr>
            </c:forEach>
          </table>
        </div>
        <div class="popup1">
          <div class="close_window">x</div>
          <label id="user" hidden></label>
          <br>
          <textarea id="text" disabled></textarea>
          <br>
          <img id="delete_msg" width="20px"  src="../images/delete.png">
        </div>
      </div>
    </div>
  </div>
  <div  class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
    <div class="panel panel-default">
      <div class="panel-body">
        <h3><fmt:message bundle="${rb}" key="office.games-history"/></h3>
        <ul id="itemContainer">
          <c:forEach var="game" items="${games}">
            <li>
                ${game.date}
              <c:if test="${game.win}"><fmt:message bundle="${rb}" key="office.win"/></c:if>
              <c:if test="${!game.win}"><fmt:message bundle="${rb}" key="office.lose"/></c:if>
                ${game.bet}$
            </li>
          </c:forEach>
        </ul>
        <div class="holder"></div>
      </div>
    </div>
  </div>
</div>
<jsp:directive.include file="../WEB-INF/jspf/scripts.jspf" />
<script src="../js/user_office.js"></script>
<script src="../js/lib/jPages.js"></script>
<script>
    $(function(){
        $("div.holder").jPages({
            containerID : "itemContainer"
        });
    });
</script>
</body>
</html>
