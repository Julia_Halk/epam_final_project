<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<fmt:setLocale value="${lang}" scope="session" />
<fmt:setBundle basename="messages" var="rb"/>
<!DOCTYPE html>
<html>
<head>
  <title><fmt:message key="page.admin-office.title" bundle="${rb}"/></title>
  <link rel="stylesheet" href="../css/lib/bootstrap.min.css">
  <link rel="stylesheet" href="../css/main.css">
  <link rel="stylesheet" href="../css/lib/jPages.css">
  <link rel="stylesheet" href="../css/office.css">
  <link rel="stylesheet" href="../css/admin_office.css">

</head>
<body>
<div class="mainbody container-fluid">
  <div class="main_row">
    <div class="navbar navbar-inverse navbar-static-top" role="navigation">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          </button>
          <a class="navbar-brand" href="#"><img src="../images/logo.png"> </a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <span class="user-avatar pull-left" style="margin-right:8px; margin-top:-5px;">
                    <img src="../images/photos/admin.jpg" class="img-responsive img-circle" width="30px" height="30px">
                  </span>
              <span class="user-name">Admin</span>
              <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li>
                  <div class="navbar-footer">
                    <div class="navbar-footer-content">
                      <ul>
                        <li><a href="controller?command=news"><fmt:message key="news" bundle="${rb}"/></a></li>
                        <li><a href="controller?command=logout"><i class="fa fa-power-off" aria-hidden="true"></i> <fmt:message key="button.logout" bundle="${rb}"/></a></li>
                      </ul>
                    </div>
                  </div>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div>
          <div class="col-lg-3 col-md-3 hidden-sm hidden-xs">
            <div class="panel panel-default">
              <div class="panel-body">
                <div class="media">
                  <div align="center">
                    <img class="thumbnail img-responsive" src="../images/photos/admin.jpg" width="300px" height="300px">
                    <div class="overlay"></div>
                  </div>
                  <div class="media-body">
                    <h1>Admin</h1>
                    <button id="add"  class="btn btn-black open_window"><fmt:message key="office.add-news" bundle="${rb}"/></button><br>
                    <div class="overlay" title="окно"></div>
                    <div class="popup">
                      <div class="close_window">x</div>
                      <form action="controller" method="post" id="news">
                        <input type="hidden" name="command" value="add_news">
                        <label for="title_ru"><fmt:message key="office.news-title-ru" bundle="${rb}"/></label><br>
                        <input id="title_ru" name="title_ru" type="text"><br>
                        <label for="text_ru"><fmt:message key="office.text-ru" bundle="${rb}"/></label><br>
                        <textarea name="text_ru" id="text_ru" class="form-control" maxlength="2000"></textarea>
                        <hr>
                        <label for="title_en"><fmt:message key="office.news-title-en" bundle="${rb}"/></label><br>
                        <input id="title_en" name="title_en" type="text"><br>
                        <label for="text_en"><fmt:message key="office.text-en" bundle="${rb}"/></label><br>
                        <textarea name="text_en" id="text_en" class="form-control" maxlength="2000"></textarea>
                        <hr>
                        <input type="submit" class="btn btn-default" value="<fmt:message key="office.button.add" bundle="${rb}"/>">
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
            <div class="panel panel-default">
              <div class="panel-body">
                <h3><fmt:message key="office.users" bundle="${rb}"/></h3>
                <ul id="itemContainer">
                  <c:forEach var="user" items="${users}">
                    <li><label class="email">${user.email}</label><label class="username">${user.username}</label>
                      <button id="ban" class='btn btn-black' onclick="setBan('${!user.ban}','${user.userId}', this)">${user.ban?"unban":"ban"}</button>
                    </li>
                  </c:forEach>
                </ul>
                <div class="holder"></div>
            </div>
          </div>
        </div>
          <div id="messages" class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
            <div class="panel panel-default">
              <div class="panel-body">
                <div>
                  <h3><fmt:message key="office.complaints" bundle="${rb}"/></h3>
                  <table id = "complaints">
                    <c:forEach var="message" items="${messages}">
                      <c:if test="${message.read}">
                        <tr class="complaint read">
                      </c:if>
                      <c:if test="${!message.read}">
                        <tr class="complaint">
                      </c:if>
                      <td class="date"><fmt:formatDate value="${message.date}" pattern="dd MMM yyyy"/></td>
                      <td class="user">${message.from.email}</td>
                      <td class="text">
                        <c:if test="${message.text.length()>=30}">
                          ${message.text.substring(0,30)}...
                        </c:if>
                        <c:if test="${message.text.length()<30}">
                          ${message.text}
                        </c:if>
                      </td>
                      <td class="hidden-text" hidden>${message.text}</td>
                      <td class="id" hidden>${message.from.userId}</td>
                      </tr>
                    </c:forEach>
                  </table>
                </div>
                <div class="popup1">
                  <div class="close_window">x</div>
                  <label id="user"></label>
                  <br>
                  <textarea id="text" disabled></textarea>
                  <br>
                  <img id="delete_msg" width="20px" style="float: right" src="../images/delete.png">
                </div>
            </div>
          </div>
        </div>
      ${errorMessage}
      ${actionMessage}
    </div>
  </div>
</div>
<jsp:directive.include file="../WEB-INF/jspf/scripts.jspf" />
<script src="../js/admin_office.js"></script>
<script src="../js/lib/jPages.js"></script>
<script>
    $(function(){
        $("div.holder").jPages({
            containerID : "itemContainer"
        });
    });
</script>
</body>
</html>
