<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<fmt:setLocale value="${lang}" scope="session" />
<fmt:setBundle basename="messages" var="rb" />
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport"    content="width=device-width, initial-scale=1.0">
    <title><fmt:message bundle="${rb}" key="reg"/></title>
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/images/gt_favicon.png">
    <link rel="stylesheet" media="screen" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/lib/bootstrap.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/lib/bootstrap-theme.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/main.css">
</head>
<style>
    body{
        background-image: url(../images/poker_by_mimiss.jpg);
    }
</style>
<body>
<c:if test="${not empty user}">
    <c:redirect url="controller?command=forward&forward=toUserOffice" />
</c:if>
<div class="navbar navbar-inverse navbar-fixed-top headroom" >
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            <a class="navbar-brand" href="#"><img src="../images/logo.png"> </a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav pull-right">
                <li><a href="../index.jsp"><fmt:message bundle="${rb}" key="main.page.title"/></a></li>
                <li><a href="controller?command=news"><fmt:message bundle="${rb}" key="news"/></a></li>
                <li><a class="btn" href="controller?command=forward&forward=toLogin"><fmt:message bundle="${rb}" key="button.signin"/> </a></li>
            </ul>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <article class="col-xs-12 maincontent">
            <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                <div class="panel panel-default sign_up">
                    <div class="panel-body">
                        <h3 class="thin text-center"><fmt:message bundle="${rb}" key="form.registration"/></h3>
                        <hr>
                        <form action="${pageContext.request.contextPath}../controller" method="post">
                            <input type="hidden" name="command" value="registration">
                            <div class="top-margin">
                                <label><fmt:message bundle="${rb}" key="register.placeholder.username"/><span class="text-danger">*</span></label>
                                <input type="text" name="username" class="form-control" autocomplete="off" required
                                       pattern="^[A-ZА-ЯЁ][a-zа-яё]{2,19}$"
                                       placeholder="<fmt:message bundle="${rb}" key="register.placeholder.username"/>"
                                       data-toggle="tooltip" title="<fmt:message bundle="${rb}" key="register.help.name"/>">
                            </div>
                            <div class="top-margin">
                                <label><fmt:message bundle="${rb}" key="register.placeholder.e-mail"/> <span class="text-danger">*</span></label>
                                <input type="email" name="email" class="form-control" autocomplete="off"
                                       pattern="^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$"
                                       placeholder="<fmt:message key="register.placeholder.e-mail" bundle="${rb}"/>" required>
                            </div>
                            <div class="row top-margin">
                                <div class="col-sm-6">
                                    <label><fmt:message bundle="${rb}" key="register.placeholder.password"/> <span class="text-danger">*</span></label>
                                    <input type="password" name="password" class="form-control"
                                           placeholder="<fmt:message key="register.placeholder.password" bundle="${rb}"/>"
                                           pattern="^[а-яА-ЯёЁa-zA-Z0-9]{4,19}$"
                                           data-toggle="tooltip" title="<fmt:message key="register.help.password" bundle="${rb}"/>" required>
                                </div>
                                <div class="col-sm-6">
                                    <label><fmt:message bundle="${rb}" key="register.title.password.again"/> <span class="text-danger">*</span></label>
                                    <input type="password" name="passwordAgain" class="form-control"
                                           placeholder="<fmt:message key="register.placeholder.password" bundle="${rb}"/>"
                                           pattern="^[а-яА-ЯёЁa-zA-Z0-9]{4,19}$" required
                                           data-toggle="tooltip" title="<fmt:message key="register.help.password" bundle="${rb}"/>">
                                </div>
                            </div>
                            <hr>
                            <div>
                                <label><fmt:message bundle="${rb}" key="register.gender"/> <span class="text-danger">*</span></label><br>
                                <label class="radio-inline"><input type="radio" value="male" name="gender"><fmt:message key="register.gender.male" bundle="${rb}"/></label>
                                <label class="radio-inline"><input type="radio" value="female" name="gender"><fmt:message key="register.gender.female" bundle="${rb}"/></label>
                            </div>
                            <hr>
                            <div>
                                <div class="form-group">
                                    <label for="inputDate"><fmt:message key="register.date" bundle="${rb}"/></label>
                                    <input id="inputDate" name="date" type="date" class="form-control" required>
                                </div>
                            </div>
                            <c:if test="${not empty errorMessage}">
                                ${errorMessage}
                            </c:if>
                            <c:if test="${not empty actionMessage}">
                                ${actionMessage}
                            </c:if>
                            <hr>
                            <button class="btn btn-black" type="submit"><fmt:message bundle="${rb}" key="button.register"/></button>
                        </form>
                    </div>
                </div>
            </div>
        </article>
    </div>
</div>
<jsp:directive.include file="../WEB-INF/jspf/scripts.jspf" />
<script>
    $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});</script>
</body>
</html>