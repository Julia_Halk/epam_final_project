<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<fmt:setLocale value="${lang}" scope="session" />
<fmt:setBundle basename="messages" var="rb"/>
<!DOCTYPE html>
<html>
<head>
    <title>Stoss</title>
    <link rel="stylesheet" href="../css/lib/bootstrap.min.css">
    <link href="../css/game.css" rel="stylesheet">
</head>
<body>
<div class="cards_container">
    <div id="modal_form">
        <span id="modal_close">X</span>
        <label for="score"><fmt:message key="office.score" bundle="${rb}"/></label><br>
        <input type="text" id="score" value="${user.score}" disabled>
        <label for="bet"><fmt:message key="bet" bundle="${rb}"/></label><br>
        <input id="bet" type="number" min="1" max="1000"><br>
        <div id="message"><fmt:message key="not.enough.money.message" bundle="${rb}"/></div><br>
        <div id="small_int_message"><fmt:message key="incorrect.sum" bundle="${rb}"/></div><br>
        <button id="play" class="btn btn-black"><fmt:message key="game.play" bundle="${rb}"/></button>
    </div>
<div id="modal_form1">
    <span id="modal_close1">X</span>
    <div id="result"></div>
    <button id="play_more" class="btn btn-black"><fmt:message key="game.play-more" bundle="${rb}"/></button>
</div>
<div id="overlay"></div>
<div id="cards">
    <img src="../images/cards/Atlas_deck_6_of_hearts.svg.png">
    <img src="../images/cards/Atlas_deck_7_of_hearts.svg.png">
    <img src="../images/cards/Atlas_deck_8_of_hearts.svg.png">
    <img src="../images/cards/Atlas_deck_9_of_hearts.svg.png">
    <img src="../images/cards/Atlas_deck_10_of_hearts.svg.png">
    <img src="../images/cards/Atlas_deck_jack_of_hearts.svg.png">
    <img src="../images/cards/Atlas_deck_queen_of_hearts.svg.png">
    <img src="../images/cards/Atlas_deck_king_of_hearts.svg.png">
    <img src="../images/cards/Atlas_deck_ace_of_hearts.svg.png">
    <img src="../images/cards/Atlas_deck_6_of_diamonds.svg.png">
    <img src="../images/cards/Atlas_deck_7_of_diamonds.svg.png">
    <img src="../images/cards/Atlas_deck_8_of_diamonds.svg.png">
    <img src="../images/cards/Atlas_deck_9_of_diamonds.svg.png">
    <img src="../images/cards/Atlas_deck_10_of_diamonds.svg.png">
    <img src="../images/cards/Atlas_deck_jack_of_diamonds.svg.png">
    <img src="../images/cards/Atlas_deck_queen_of_diamonds.svg.png">
    <img src="../images/cards/Atlas_deck_king_of_diamonds.svg.png">
    <img src="../images/cards/Atlas_deck_ace_of_diamonds.svg.png">
    <img src="../images/cards/Atlas_deck_6_of_clubs.svg.png">
    <img src="../images/cards/Atlas_deck_7_of_clubs.svg.png">
    <img src="../images/cards/Atlas_deck_8_of_clubs.svg.png">
    <img src="../images/cards/Atlas_deck_9_of_clubs.svg.png">
    <img src="../images/cards/Atlas_deck_10_of_clubs.svg.png">
    <img src="../images/cards/Atlas_deck_jack_of_clubs.svg.png">
    <img src="../images/cards/Atlas_deck_queen_of_clubs.svg.png">
    <img src="../images/cards/Atlas_deck_king_of_clubs.svg.png">
    <img src="../images/cards/Atlas_deck_ace_of_clubs.svg.png">
    <img src="../images/cards/Atlas_deck_6_of_spades.svg.png">
    <img src="../images/cards/Atlas_deck_7_of_spades.svg.png">
    <img src="../images/cards/Atlas_deck_8_of_spades.svg.png">
    <img src="../images/cards/Atlas_deck_9_of_spades.svg.png">
    <img src="../images/cards/Atlas_deck_10_of_spades.svg.png">
    <img src="../images/cards/Atlas_deck_jack_of_spades.svg.png">
    <img src="../images/cards/Atlas_deck_queen_of_spades.svg.png">
    <img src="../images/cards/Atlas_deck_king_of_spades.svg.png">
    <img src="../images/cards/Atlas_deck_ace_of_spades.svg.png">
</div>
<div id="choose">
    <img id="6h" src="../images/cards/Atlas_deck_6_of_hearts.svg.png">
    <img id="7h" src="../images/cards/Atlas_deck_7_of_hearts.svg.png">
    <img id="8h" src="../images/cards/Atlas_deck_8_of_hearts.svg.png">
    <img id="9h" src="../images/cards/Atlas_deck_9_of_hearts.svg.png">
    <img id="10h" src="../images/cards/Atlas_deck_10_of_hearts.svg.png">
    <img id="jh" src="../images/cards/Atlas_deck_jack_of_hearts.svg.png">
    <img id="qh" src="../images/cards/Atlas_deck_queen_of_hearts.svg.png">
    <img id="kh" src="../images/cards/Atlas_deck_king_of_hearts.svg.png">
    <img id="ah" src="../images/cards/Atlas_deck_ace_of_hearts.svg.png">
    <img id="6d" src="../images/cards/Atlas_deck_6_of_diamonds.svg.png">
    <img id="7d" src="../images/cards/Atlas_deck_7_of_diamonds.svg.png">
    <img id="8d" src="../images/cards/Atlas_deck_8_of_diamonds.svg.png">
    <img id="9d" src="../images/cards/Atlas_deck_9_of_diamonds.svg.png">
    <img id="10d" src="../images/cards/Atlas_deck_10_of_diamonds.svg.png">
    <img id="jd" src="../images/cards/Atlas_deck_jack_of_diamonds.svg.png">
    <img id="qd" src="../images/cards/Atlas_deck_queen_of_diamonds.svg.png">
    <img id="kd" src="../images/cards/Atlas_deck_king_of_diamonds.svg.png">
    <img id="ad" src="../images/cards/Atlas_deck_ace_of_diamonds.svg.png">
    <img id="6c" src="../images/cards/Atlas_deck_6_of_clubs.svg.png">
    <img id="7c" src="../images/cards/Atlas_deck_7_of_clubs.svg.png">
    <img id="8c" src="../images/cards/Atlas_deck_8_of_clubs.svg.png">
    <img id="9c" src="../images/cards/Atlas_deck_9_of_clubs.svg.png">
    <img id="10c" src="../images/cards/Atlas_deck_10_of_clubs.svg.png">
    <img id="jc" src="../images/cards/Atlas_deck_jack_of_clubs.svg.png">
    <img id="qc" src="../images/cards/Atlas_deck_queen_of_clubs.svg.png">
    <img id="kc" src="../images/cards/Atlas_deck_king_of_clubs.svg.png">
    <img id="ac" src="../images/cards/Atlas_deck_ace_of_clubs.svg.png">
    <img id="6s" src="../images/cards/Atlas_deck_6_of_spades.svg.png">
    <img id="7s" src="../images/cards/Atlas_deck_7_of_spades.svg.png">
    <img id="8s" src="../images/cards/Atlas_deck_8_of_spades.svg.png">
    <img id="9s" src="../images/cards/Atlas_deck_9_of_spades.svg.png">
    <img id="10s" src="../images/cards/Atlas_deck_10_of_spades.svg.png">
    <img id="js" src="../images/cards/Atlas_deck_jack_of_spades.svg.png">
    <img id="qs" src="../images/cards/Atlas_deck_queen_of_spades.svg.png">
    <img id="ks" src="../images/cards/Atlas_deck_king_of_spades.svg.png">
    <img id="as" src="../images/cards/Atlas_deck_ace_of_spades.svg.png">
</div>
</div>
<br>
<script src="../js/lib/jquery-3.2.1.min.js"></script>
<script src="../js/lib/bootstrap.min.js"></script>
<jsp:directive.include file="../WEB-INF/jspf/scripts.jspf" />
<script src="../js/game.js"></script>
</body>
</html>
