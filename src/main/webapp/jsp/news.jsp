<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${lang}" scope="session" />
<fmt:setBundle basename="messages" var="rb"/>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport"    content="width=device-width, initial-scale=1.0">
    <title><fmt:message bundle="${rb}" key="news"/> </title>
    <link rel="shortcut icon" href="../images/gt_favicon.png">
    <link rel="stylesheet" media="screen" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/lib/bootstrap.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/lib/bootstrap-theme.css">
    <link rel="stylesheet" href="../css/main.css">
    <link rel="stylesheet" href="../css/lib/jPages.css">
    <style>
        body{
            background-image: url(../images/casino-background.jpg);
            background-size: cover;
        }
    </style>
</head>
<body>
<div class="navbar navbar-inverse navbar-fixed-top headroom" >
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            <a class="navbar-brand" href="#"><img src="../images/logo.png"> </a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav pull-right">
                <c:if test="${not empty user}">
                    <li><a href="controller?command=logout"><fmt:message key="button.logout" bundle="${rb}"/></a></li>
                    <li><a href="controller?command=forward&forward=toUserOffice"><fmt:message key="button.private-office" bundle="${rb}"/></a></li>
                </c:if>
                <c:if test="${empty user}">
                    <li>
                        <a href="controller?command=forward&forward=toLogin"><fmt:message key="in_up" bundle="${rb}"/></a>
                    </li>
                </c:if>
                <li><a href="http://localhost:8080"><fmt:message bundle="${rb}" key="header.main"/></a></li>
            </ul>
        </div>
    </div>
</div>
<div id="main" class="main">
    <ul id="itemContainer">
        <c:forEach var="news" items="${news}">
            <li> <h3 style="color: goldenrod;font-style: italic">${news.title}</h3>
                <p >${news.text}</p>
                <hr>
            </li>
        </c:forEach>
    </ul>
    <div class="holder"> </div>
</div>
<jsp:directive.include file="../WEB-INF/jspf/scripts.jspf" />
<script src="../js/lib/jPages.js"></script>
<script>
    $(function(){
        $("div.holder").jPages({
            containerID : "itemContainer"
        });
    });
</script>
</body>
</html>
