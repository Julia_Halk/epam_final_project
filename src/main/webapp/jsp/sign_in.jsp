<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${lang}" scope="session" />
<fmt:setBundle basename="messages" var="rb" />
<html>
<head>
  <title><fmt:message bundle="${rb}" key="form.signin.enter-in-account"/></title>
  <meta charset="utf-8">
  <link rel="shortcut icon" href="${pageContext.request.contextPath}/images/gt_favicon.png">
  <link rel="shortcut icon" href="${pageContext.request.contextPath}/images/gt_favicon.png">
  <link rel="stylesheet" media="screen" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/css/lib/bootstrap.min.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/css/lib/bootstrap-theme.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/css/main.css">
</head>
<style>
  body{
    background-image: url(../images/poker_by_mimiss.jpg);
  }
</style>
<body>
<c:if test="${not empty user}">
  <c:redirect url="controller?command=forward&forward=toUserOffice" />
</c:if>
<div class="navbar navbar-inverse navbar-fixed-top headroom" >
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <a class="navbar-brand" href="#"><img src="../images/logo.png"> </a>
    </div>
    <div class="navbar-collapse collapse">
      <ul class="nav navbar-nav pull-right">
        <li><a href="../index.jsp"><fmt:message bundle="${rb}" key="header.main"/> </a></li>
        <li><a href="controller?command=news"><fmt:message bundle="${rb}" key="news"/> </a></li>
        <li><a class="btn" href="controller?command=forward&forward=toRegister"><fmt:message bundle="${rb}" key="reg"/> </a></li>
      </ul>
    </div>
  </div>
</div>
<div class="container">
  <div class="row">
    <article class="col-xs-12 maincontent">
      <div class="col-md-6 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
        <div class="panel panel-default">
          <div class="panel-body">
            <h3 class="thin text-center"><fmt:message bundle="${rb}" key="form.signin.enter-in-account"/></h3>
            <hr>
            <form action = "controller" method="post">
              <input type="hidden" name="command" value="login">
              <div class="top-margin">
                <label for="email">Email<span class="text-danger">*</span></label>
                <input id="email" name="email" type="email" class="form-control"
                       pattern="^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$">
              </div>
              <div class="top-margin">
                <label for="password"><fmt:message bundle="${rb}" key="register.placeholder.password"/><span class="text-danger">*</span></label>
                <input id="password" name="password" type="password" class="form-control"
                       pattern="^[а-яА-ЯёЁa-zA-Z0-9-_\.]{4,19}$">
              </div>
              <c:if test="${not empty errorMessage}">
                ${errorMessage}
              </c:if>
              <c:if test="${not empty actionMessage}">
                ${actionMessage}
              </c:if>
              <hr>
                  <button class="btn btn-black" type="submit"><fmt:message bundle="${rb}" key="button.signin"/></button>
            </form>
          </div>
        </div>
      </div>
    </article>
  </div>
</div>
</body>
</html>