<%@ page isErrorPage="true" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
    <title>Error Page</title>
</head>
<style>
    body{
        overflow: scroll;
        background-image: url("../images/background.jpg");
        background-size: cover;
        color: #DDD;
        font-family: 'Helvetica', 'Lucida Grande', 'Arial', sans-serif;
    }
</style>
<body>
Request from ${pageContext.errorData.requestURI} is failed
<br/>
Servlet name or type: ${pageContext.errorData.servletName}
<br/>
Status code: ${pageContext.errorData.statusCode}
<br/>
Exception: ${pageContext.errorData.throwable}
<br>
<a href="../index.jsp">Back</a>
</body></html>
