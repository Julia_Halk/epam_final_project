var imgarray = document.getElementById("choose").getElementsByTagName("img");
var cards= [].slice.call(document.getElementById("cards").getElementsByTagName("img")),
len = cards.length,
elem = document.createElement("img");
var card;

$(document).ready(function(){
    $('#message').hide();
    $('#small_int_message').hide();
    $('#overlay').fadeIn(400,
        function(){
            $('#modal_form')
                .css('display', 'block')
                .animate({opacity: 1, top: '50%'}, 200);
        });
    for ( i = 0; i < len; i++) {
        var rand = Math.floor(Math.random() * len);
        cards[i].parentNode.replaceChild(elem, cards[i]);
        i != rand && cards[rand].parentNode.replaceChild(cards[i], cards[rand]);
        elem.parentNode.replaceChild(cards[rand], elem);
    }
    cards = document.getElementById("cards").getElementsByTagName("img");
    for(var i=0; i<cards.length;i++){
        cards[i].style.top="0";
        cards[i].style.left="0";
    }
    for (i = 0; i < imgarray.length; i++) {
        imgarray[i].onclick = function () {
            card = this;
            hide(card);
            game(card);
        };
    }
});
function showResult(){
    $('#overlay').fadeIn(400, // снaчaлa плaвнo пoкaзывaем темную пoдлoжку
        function(){ // пoсле выпoлнения предъидущей aнимaции
            $('#modal_form1')
                .css('display', 'block') // убирaем у мoдaльнoгo oкнa display: none;
                .animate({opacity: 1, top: '50%'}, 200); // плaвнo прибaвляем прoзрaчнoсть oднoвременнo сo съезжaнием вниз
        });
    $('label #bet').val(bet);
}
function game(card) {
    var i = cards.length-1;
    while (cards.length >= 0) {
            move(cards[i]);
            if (cards[i].src == card.src) {
                showResult();
                sentData(bet, "true");
                break;
            }
            else if (cards[i - 1].src == card.src) {
                showResult();
                sentData(bet, "false");
                break;
            }
            else {
                    cards[i].style.display = "none";
                    cards[i - 1].style.display = "none";
                    i -= 2;
            }
    }
}
function sentData(bet,win) {
    if(win=="true") {
        $('#modal_form1').css({'background': '#fff url(../images/giphy.gif)', 'backgroundSize':'cover'});
    }
    else{
        $('#modal_form1').css({'background': '#fff url(../images/lose.gif)', 'backgroundSize':'cover'});
    }
    var data = function (bet, win) {
        return {
            command: "addGame",
            bet: bet,
            win: win
        };
    };
    $.ajax({
        type: "POST",
        url: "ajaxController",
        data: JSON.stringify(data(bet, win)),
        dataType: "json",
        async: false,
        headers: {"Access-Control-Allow-Origin": "*"},
        contentType: "application/json; charset=utf-8",
        success: function (responseText) {
        },
        error: function (responseText) {

        }
    });
}
$('#modal_close, #modal_close1').click(function(){
    $(location).attr('href', 'controller?command=forward&forward=toUserOffice');
});
$('#play_more').click(function(){
    location.reload()
});
var bet;
$('#play').click( function(){
    var user_bet=parseInt($('#bet').val());
    var score = parseInt($('#score').val());
    if((user_bet<=score)&&(user_bet>0)) {
        bet = user_bet.toString();
        $('#modal_form')
            .animate({opacity: 0, top: '45%'}, 200,
            function () {
                $(this).css('display', 'none');
                $('#overlay').fadeOut(400);
            }
        );
    }
    else if(user_bet>score){
        $('#small_int_message').hide();
        $('#message').show();
    }
    else if(user_bet<0){
        $('#message').hide();
        $('#small_int_message').show();
    }
});
function move(card) {
    card.style.left = parseInt(card.style.left) + 20 + "px";
}

function shuffel() {
    console.log("shuffel");
    var shuffle = cards.sort(function (a, b) {
        return (Math.random() * 10)-5;
    });
    for (var i = 0; i < cards.length; i++) {
        cards[i].src = shuffle[i].src;
        console.log(cards[i].src);
    }
}

function hide(card) {
    for (var i = 0; i < imgarray.length; i++) {
        if (card != imgarray[i])
            imgarray[i].style.display = 'none';
    }
}
