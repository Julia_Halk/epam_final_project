$(document).ready(function() {
    $("#tab2").hide();
    $("#tab3").hide();
    $("#users").click(function(){
        $("#tab2").show();
        $("#tab3").hide();
    });
    $("#messages").click(function(){
        $("#tab2").hide();
        $("#tab3").show();
    });
});
function setBan(ban, userId, button){
    var text = ($(button).text()=="ban")?"unban":"ban";
    $(button).text(text);
    var data = function () {
        return {
            command: "setBan",
            ban: ban,
            user: userId
        };
    };
    $.ajax({
        type: "POST",
        url: "ajaxController",
        data: JSON.stringify(data()),
        dataType: "json",
        async: false,
        headers: {"Access-Control-Allow-Origin": "*"},
        contentType: "application/json; charset=utf-8",
        success: function (responseText) {
        },
        error: function (responseText) {
        }
    });
}
$('.close_window, .overlay').click(function (){
    $(".clicked").attr("class", "read");
    $('.popup, .popup1, .popup2, .overlay').css({'opacity': 0, 'visibility': 'hidden'});
});
$('.open_window').click(function (e){
    $('.popup, .overlay').css({'opacity': 1, 'visibility': 'visible'});
    e.preventDefault();
});
var index ;
$('#delete_msg').click(function () {
    var text = $('#text').val();
    var data = function () {
        return {
            command: "deleteMessage",
            text: text,
            user: id
        };
    };
    $.ajax({
        type: "POST",
        url: "ajaxController",
        data: JSON.stringify(data()),
        dataType: "json",
        async: false,
        headers: {"Access-Control-Allow-Origin": "*"},
        contentType: "application/json; charset=utf-8",
        success: function () {
        },
        error: function () {
        }
    });
    $(".clicked").remove();
    $('.popup, .popup1, .overlay').css({'opacity': 0, 'visibility': 'hidden'});
});
