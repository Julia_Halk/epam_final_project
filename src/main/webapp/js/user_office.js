$(document).ready(function() {
    $("#tab2").hide();
    $("#tab3").hide();
    $("#tab4").hide();
    $("#tab5").hide();
    $("#tab6").hide();
    $("#addMoney").click(function(){
        $("#tab2").show();
        $("#tab4").hide();
        $("#tab3").hide();
        $("#tab5").hide();
        $("#tab6").hide();
    });
    $("#withdrawMoney").click(function(){
        $("#tab2").hide();
        $("#tab4").hide();
        $("#tab3").show();
        $("#tab5").hide();
        $("#tab6").hide();
    });
    $("#gamesHistory").click(function(){
        $("#tab2").hide();
        $("#tab3").hide();
        $("#tab4").show();
        $("#tab5").hide();
        $("#tab6").hide();
    });
    $("#changePassword").click(function(){
        $("#tab2").hide();
        $("#tab3").hide();
        $("#tab4").hide();
        $("#tab5").show();
        $("#tab6").hide();
    });
    $("#messages").click(function(){
        $("#tab2").hide();
        $("#tab3").hide();
        $("#tab4").hide();
        $("#tab5").hide();
        $("#tab6").show();
    });
    $(function(){
        $("[type=file]").on("change", function(){
            var file = this.files[0];
            var formdata = new FormData();
            formdata.append("file", file);
            if(file.name.length >= 30){
                $('label').text(file.name.substr(0,30) + '..');
            }else {
                $('label').text(file.name);
            }
            var ext = $('#file').val().split('.').pop().toLowerCase();
            if($.inArray(ext, ['php', 'html']) !== -1) {
                $('#file').val('');
                alert('This file extension is not allowed!');
            }
        });

    });
});
$('#upload').click(function () {
   $('#newImage').show();
});
$('#changePhoto').click(function () {
    $('#newImage').hide();
});
$('.popup .close_window,.popup1 .close_window, .popup2 .close_window, .overlay').click(function (){
    $('.popup, .popup1, .popup2, .overlay').css({'opacity': 0, 'visibility': 'hidden'});
});
$('.open_window').click(function (e){
    $('.popup, .overlay').css({'opacity': 1, 'visibility': 'visible'});
    e.preventDefault();
});
$('.open_window1').click(function (e){
    $('.popup2, .overlay').css({'opacity': 1, 'visibility': 'visible'});
    e.preventDefault();
});
$("#edit_username").click(function(){
$("#username").removeAttr('disabled');
    this.hidden = true;
    $("#save_username").removeAttr('hidden');
});

$("#save_username").click(function(){
    var data = function (username) {
        return {
            command: "changeUsername",
            username: username
        };
    };
    var username = $("#username").val();
    $.ajax({
        type: "POST",
        url: "ajaxController",
        data: JSON.stringify(data(username)),
        dataType: "json",
        async: false,
        headers: {"Access-Control-Allow-Origin": "*"},
        contentType: "application/json; charset=utf-8",
        success: function (responseText) {
            $("#save_username").hidden=true;
            $("#edit_username").hidden=false;
            $("#username").disabled=true;
        },
        error: function (responseText) {

        }
    });
    window.location.reload(true);
});
$("#upBalance").click(function () {
    var sum = $("#money").val();
    updateBalance("add", sum);
});
$("#downBalance").click(function () {
    var sum = $("#sum").val();
    updateBalance("reduce", sum);
});
function updateBalance(action, balance) {
    var data = function (sum) {
        return {
            command: "replenishBalance",
            action: action,
            sum: sum
        };
    };
    $.ajax({
        type: "POST",
        url: "ajaxController",
        data: JSON.stringify(data(balance)),
        dataType: "json",
        async: false,
        headers: {"Access-Control-Allow-Origin": "*"},
        contentType: "application/json; charset=utf-8",
        success: function (responseText) {
            $("#money").val("");
            $(".balance").before(alertSuccess(responseText.text)).show();
        },
        error: function (responseText) {
            $("#money").val("");
            $(".balance").before(alertSuccess(responseText.text)).show();
        }
    });
    location.reload();
}