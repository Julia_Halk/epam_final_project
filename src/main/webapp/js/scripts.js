/*language dropdown*/
function DropDown(el) {
    this.dd = el;
    this.initEvents();
}

DropDown.prototype = {
    initEvents : function() {
        var obj = this;

        obj.dd.on('click', function(event){
            $(this).toggleClass('active');
            //return false;
        });
    }
};

$(document).ready(function() {
    var dd = new DropDown($('#dd'));

    $("[data-tooltip]").mousemove(function (eventObject) {

        $data_tooltip = $(this).attr("data-tooltip");

        $("#tooltip").text($data_tooltip)
            .css({
                "top": eventObject.pageY + 5,
                "left": eventObject.pageX + 5
            })
            .show();

    }).mouseout(function () {
        $("#tooltip").hide()
            .text("")
            .css({
                "top": 0,
                "left": 0
            });
    });
});

    var alertWarning = function (message) {
        return '<div class="alert alert-warning alert-dismissible fade in result" role="alert">' +
            '  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>' +
            '  <span class="result-text">' + message + '</span></div>';
    };

    var alertError = function (message) {
        return '<div class="alert alert-error alert-dismissible fade in result" role="alert">' +
            '  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>' +
            '  <span class="result-text">' + message + '</span></div>';
    };

    var alertSuccess = function (message) {
        return '<div class="alert alert-success alert-dismissible fade in result" role="alert">' +
            '  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>' +
            '  <span class="result-text">' + message + '</span></div>';
    };

$('.close_window, .overlay').click(function (){
    $(".clicked").attr("class", "read");
    $('.popup, .popup1,  .overlay').css({'opacity': 0, 'visibility': 'hidden'});
});
$('.open_window').click(function (e){
    $('.popup, .overlay').css({'opacity': 1, 'visibility': 'visible'});
    e.preventDefault();
});
var index ;
var id;
$('.complaint').click(function (e) {
    var text = $(this).children("td.hidden-text").text();
    var user = $(this).children("td.user").text();
    var date = $(this).children("td.date").text();
    id = $(this).children("td.id").text();
    index = $(this).index();
    $('.popup1, .overlay').css({'opacity': 1, 'visibility': 'visible'});
    e.preventDefault();
    $('#user').html(user);
    $('#text').val(text);
    if($(this).attr("class")!="read clicked") {
        var data = function () {
            return {
                command: "setRead",
                text: text,
                user: id,
                date: date
            };
        };
        $.ajax({
            type: "POST",
            url: "ajaxController",
            data: JSON.stringify(data()),
            dataType: "json",
            async: false,
            headers: {"Access-Control-Allow-Origin": "*"},
            contentType: "application/json; charset=utf-8",
            success: function (responseText) {
            },
            error: function (responseText) {
            }
        });
    }
    $(this).attr("class","read clicked");
});
$('#delete_msg').click(function () {
    var user = $('#user').text();
    var text = $('#text').val();
    var data = function () {
        return {
            command: "deleteMessage",
            text: text,
            user: user
        };
    };
    $.ajax({
        type: "POST",
        url: "ajaxController",
        data: JSON.stringify(data()),
        dataType: "json",
        async: false,
        headers: {"Access-Control-Allow-Origin": "*"},
        contentType: "application/json; charset=utf-8",
        success: function () {
        },
        error: function () {
        }
    });
    $(".clicked").remove();
    $('.popup, .popup1,  .overlay').css({'opacity': 0, 'visibility': 'hidden'});
});