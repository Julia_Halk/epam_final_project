<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<fmt:setLocale value="${lang}" scope="session" />
<fmt:setBundle basename="messages" var="rb"/>
<!DOCTYPE html>

<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title><fmt:message key="home" bundle="${rb}"/></title>
  <link rel="shortcut icon" href="${pageContext.request.contextPath}/images/gt_favicon.png">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/css/lib/bootstrap.min.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/css/main.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/css/language.css">

</head>

<body>
<div class="navbar navbar-inverse navbar-fixed-top headroom" >
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <a class="navbar-brand" href="#"><img src="images/logo.png"> </a>
    </div>
    <div class="navbar-collapse collapse">
      <ul class="nav navbar-nav pull-right">
        <li><a href="#"><fmt:message key="home" bundle="${rb}"/></a></li>
        <li><a id="window" href="#">
            <div class="window"><p><fmt:message key="rules.text" bundle="${rb}"/></p></div><fmt:message key="rules" bundle="${rb}"/>
          </a></li>
        <li><a href="${pageContext.request.contextPath}/controller?command=news"><fmt:message key="news" bundle="${rb}"/></a></li>
      <c:if test="${not empty user}">
        <li><a href="controller?command=logout"><fmt:message key="button.logout" bundle="${rb}"/></a></li>
        <li><a href="controller?command=forward&forward=toUserOffice"><fmt:message key="button.private-office" bundle="${rb}"/></a></li>
      </c:if>
      <c:if test="${empty user}">
        <li>
          <a href="controller?command=forward&forward=toLogin"><fmt:message key="in_up" bundle="${rb}"/></a>
        </li>
      </c:if>

        <li>
          <div id="dd" class="wrapper-dropdown" tabindex="1">
            <span><fmt:message key="language.title" bundle="${rb}"/></span>
            <ul class="dropdown">
              <li>
                <form class="navbar-form" action="controller" method="post">
                  <input type="hidden" name="command" value="language">
                  <input type="hidden" name="lang" value="ru_RU">
                  <input class="language" id="ru" type="submit" name="rus" value="<fmt:message key="language.russian" bundle="${rb}"/>">
                </form>
              </li>
              <li>
                <form class="navbar-form"  action="${pageContext.request.contextPath}/controller" method="post">
                  <input type="hidden" name="command" value="language">
                  <input type="hidden" name="lang" value="en_EN">
                  <input class="language" id="eng" type="submit" name="eng" value="<fmt:message key="language.english" bundle="${rb}"/>">
                </form>
              </li>
            </ul>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>
<header id="head">
</header>
<main>
  <div class="main">
    <div class="main_text">
      <h1><fmt:message key="home.name" bundle="${rb}"/></h1>
      <p><fmt:message key="home.text" bundle="${rb}"/></p>
    </div>
  </div>
</main>
<jsp:directive.include file="WEB-INF/jspf/footer.jspf" />
</body>
<jsp:directive.include file="WEB-INF/jspf/scripts.jspf" />
</html>